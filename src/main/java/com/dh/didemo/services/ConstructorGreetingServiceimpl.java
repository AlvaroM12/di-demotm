


package com.dh.didemo.services;

import org.springframework.stereotype.Service;

@Service
public class ConstructorGreetingServiceimpl implements GreetingService {

    public static final String GREETING = "Hello ConstructorGreetingServiceimpl";

    @Override
    public String sayGreeting() {
        return GREETING;
    }
}
