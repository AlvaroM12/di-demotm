


package com.dh.didemo.services;

import org.springframework.stereotype.Service;

@Service
public class GetterGreetingServiceimpl implements GreetingService {

    public static final String GREETING = "Hello GetterGreetingServiceimpl";

    @Override
    public String sayGreeting() {
        return GREETING;
    }
}
