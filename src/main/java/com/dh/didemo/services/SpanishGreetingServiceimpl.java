


package com.dh.didemo.services;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Primary
@Profile("es")
public class SpanishGreetingServiceimpl implements GreetingService {

    public static final String GREETING = "Hello SpanishGreetingServiceimpl";

    @Override
    public String sayGreeting() {
        return GREETING;
    }
}
