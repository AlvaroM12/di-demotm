package com.dh.didemo;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
/*@PropertySources({
        @PropertySource("classpath:database.properties"),
        @PropertySource("classpath:jms.properties")
}
)*/
public class PropertyConfig {

    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();

    }
}
