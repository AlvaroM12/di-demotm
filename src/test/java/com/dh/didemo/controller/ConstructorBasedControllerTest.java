package com.dh.didemo.controller;

import com.dh.didemo.Forecast;
import com.dh.didemo.services.GreetingService;
import com.dh.didemo.services.GreetingServiceimpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ConstructorBasedControllerTest {
    private ConstructorBasedController constructorBasedController;
    private Forecast forecast;

    @Before
    public void setUp() throws Exception {
        System.out.println("@Before");
        GreetingService greetingService = new GreetingServiceimpl();
        forecast = new Forecast();
        constructorBasedController = new ConstructorBasedController(greetingService, forecast);
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("@After");
    }

    @Test
    public void sayHello() {
        System.out.println("@Test sayHello");
        String greeting = constructorBasedController.sayHello();
        Assert.assertEquals(GreetingServiceimpl.GREETING, greeting);
    }
}