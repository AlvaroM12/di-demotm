package com.dh.didemo.controller;


import com.dh.didemo.services.GreetingServiceimpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PropertyBasedControllerTest {
    private PropertyBasedController propertyBasedController;

    @Before
    public void before() {
        propertyBasedController = new PropertyBasedController();
        propertyBasedController.greetingService = new GreetingServiceimpl();
    }

    @Test
    public void sayHello() {
        String greeting = propertyBasedController.sayHello();
        Assert.assertEquals(GreetingServiceimpl.GREETING, greeting);
    }
}